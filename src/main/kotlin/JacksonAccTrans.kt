data class AccTrans(
    val id: String,
    val des: String,
    val account: String,
    val typ: String,
    val restValue: Int,
    var checkDate: String
){
    var extraDeptBody = ExtraDeptBody(0.0, "")
}
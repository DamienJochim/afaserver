
data class ExtraDeptBody(
        val value: Double,
        val reason: String
)
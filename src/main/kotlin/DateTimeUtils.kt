import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.time.LocalDate


/**
 * Convert {@link java.time.LocalDate} to {@link org.joda.time.DateTime}
 */
fun LocalDate.toDateTime(): DateTime {
    return DateTime(DateTimeZone.UTC).withDate(
            this.getYear(), this.getMonthValue(), this.getDayOfMonth()
    ).withTime(0, 0, 0, 0);
}

/**
 * Convert [org.joda.time.DateTime] to [java.time.LocalDate]
 */
fun DateTime.DateTimetoLocalDate(): LocalDate {
    val dateTimeUtc = this.withZone(DateTimeZone.UTC)
    return LocalDate.of(dateTimeUtc.getYear(), dateTimeUtc.getMonthOfYear(), dateTimeUtc.getDayOfMonth())
}

fun DateTime.printDate(): String{
    return this.dayOfMonth().asString + "." + this.monthOfYear().asString + "." + this.year().asString
}

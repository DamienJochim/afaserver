import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class Server {

    val checkList = ArrayList<AccTrans>()
    val checkedList = ArrayList<String>()
    var sessionID: UUID? = null

    /**
     * implementiert den server und legt das routing fest
     */
    val server = embeddedServer(Jetty, 8080){
        install(ContentNegotiation){
            jackson{
                enable(SerializationFeature.INDENT_OUTPUT) //PrettyPrint der JSON Daten
            }
        }
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            header(HttpHeaders.Authorization)
            allowCredentials = true
            anyHost()
        }
        routing {
            route("/inventory") {
                get("/start") {
                    if (sessionID == null) {
                        sessionID = UUID.randomUUID()
                        call.respond(mapOf("sessionID" to sessionID))
                    }
                }
                post("/stop") {
                    val post = call.receive<Map<String, String>>()
                    if (post.getValue("UUID") == sessionID.toString()) {
                        sessionID = null
                        call.respond("STOPPED")
                    }
                }
            }
            route("/toCheck"){
                get("/{sid}") {
                    val id = call.parameters["sid"]
                    if (id == sessionID.toString()) {
                        call.respond(synchronized(checkList){ checkList.toList() })
                    }
                }
                post {
                    val post = call.receive<Map<String, List<LinkedHashMap<String, Any>>>>()
                    if(post.isNotEmpty()) {
                        val items = post.map { UUID.fromString(it.key) to it.value }.first()
                        if (items.first == sessionID) {
                            val d = items.second.map { AccTrans(it["id"].toString(), it["des"].toString(), it["account"].toString(), it["typ"].toString(), it["restValue"].toString().toInt(), it["checkDate"].toString()) }
                            checkList.addAll(d)
                            call.respond(mapOf("OK" to true))
                        }
                    }
                }
            }
            get("/checked/{sID}"){
                val id = call.parameters["sid"]
                if (id == sessionID.toString()) {
                    call.respond(synchronized(checkedList) { checkedList.toList() })
                }
            }
            post("/check/{id}"){
                val id = call.parameters["id"]
                val post = call.receive<Map<String, String>>()
                if (id != null && post["sessionID"] == sessionID.toString()){
                    checkedList += id
                    call.respond(mapOf("OK" to true))
                } else {
                    call.respond("ERR_NO_ID")
                }
            }
        }
    }

    fun start(){
        server.start(wait = true)
    }

    fun stop(){
        server.stop(5, 50, TimeUnit.SECONDS)
    }

}

/**
 * startet den server
 */
fun main(args: Array<String>) {
    val server = Server()
    server.start()
}